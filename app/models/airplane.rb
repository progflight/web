class Airplane < ActiveRecord::Base
	belongs_to :pilot
	has_many :plans
end