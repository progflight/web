#Defines table for user
class Pilot < ActiveRecord::Base
	validates :name, presence: true, length: { minimum: 2, too_short: "%{count} characters is the minimum allowed" }
	enum rating: [ "Private Pilot", "Commercial Pilot", "CFI", "CFII", "ATP" ]
	has_many :airplanes
	validates :password, presence: true, length: { minimum: 8, maximum: 90 },
  confirmation: true,
  if: :password
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
	validates :email, presence: true, length: { maximum:105 },
			         uniqueness: {case_sensitive: false },
							 format: { with: VALID_EMAIL_REGEX }
	has_secure_password

	def self.find_or_create_from_auth_hash(auth_hash)
    pilot = where(provider: auth_hash.provider, uid: auth_hash.uid).first_or_create
    pilot.update(
      name: auth_hash.info.name,
      token: auth_hash.credentials.token,
      secret: auth_hash.credentials.secret,
      email: auth_hash.info.email,
      password: auth_hash.credentials.token,
      from_twitter: true
    )
    pilot
  end

end
