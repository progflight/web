class Plan < ActiveRecord::Base
	belongs_to :airplane
	has_many :waypoints
end