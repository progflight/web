# User profile controller (create user and profile setup form)

class PilotsController < ApplicationController

	before_action :require_user, except: [:new, :create]
	before_action :require_same_user, only: [:edit, :update]
	before_action :require_logged_out, only: [:new, :create]

	def index
	end

	def show
		@pilot = Pilot.find(params[:id])
	end

	# Add age to the pilot (user)
	def new
		@pilot = Pilot.new
		 @years = 1999.downto(1935)
		 @months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
		 @days = 1..31
	end

	# If the user has already set up the profile, do not show this form (redirect to flight planing)
	def initial
		@pilot = Pilot.find(params[:id])
		if @pilot.setup_complete || @pilot.id != current_user.id
			redirect_to '/plans/new'
		end
	end

	def initial_fail(message)
		flash[:danger] = message
		redirect_to '/pilots/' + current_user.id.to_s + '/initial'
		return
	end

	# store initial profile setup in DB, if fields are 
	# missing or input is invalid, display issue to user
	def initial_update

		@pilot = Pilot.find(params[:id])

		# set required fiels of profile setup to defult values
		certificate = 0
		base_icao = "KBED"
		instrument_rated = false
		
		airplane_vx = 65
		airplane_vy = 80
		airplane_vg = 80
		airplane_tailnumber = "N12345"
		airplane_make = "Cessna 172M"

		if params[:certificate] && is_number(params[:certificate])
			tmp = params[:certificate].to_i
			if tmp >= 0 && tmp <= 4
				certificate = params[:certificate].to_i
			else
				initial_fail('Please enter a valid pilot certificate')
				return
			end
		else
			initial_fail('We need your certificate information to continue')
			return
		end

		if params[:instrument_rated] && is_number(params[:instrument_rated])
			tmp = params[:instrument_rated].to_i
			if tmp == 0 || tmp == 1
				tmp == 0 ? instrument_rated = true : instrument_rated = false
			else
				initial_fail('We need your instrument rating information')
				return
			end
		else
			initial_fail('We need your instrument rating information')
			return
		end

		if params[:base] && params[:base].length == 4
			base_icao = params[:base]
		else
			initial_fail('Please enter a valid home airport ICAO identifier')
			return
		end

		# Airplane validation follows
		if params[:tailnumber] && params[:tailnumber].length <= 6 && params[:tailnumber].length >= 3 && is_number(params[:tailnumber][1])
			airplane_tailnumber = params[:tailnumber]
		else
			initial_fail('Please enter a valid airplane tailnumber')
			return
		end

		if params[:vx] && is_number(params[:vx])
			airplane_vx = params[:vx].to_i
		else
			initial_fail('Please enter a valid best angle of climb speed')
			return
		end

		if params[:vy] && is_number(params[:vy])
			airplane_vy = params[:vy].to_i
		else
			initial_fail('Please enter a valid best rate of climb speed')
			return
		end

		if params[:vg] && is_number(params[:vg])
			airplane_vg = params[:vg].to_i
		else
			initial_fail('Please enter a valid best glide speed')
			return
		end

		if params[:make]
			airplane_make = params[:make]
		else
			initial_fail("Please enter a valid airplane make")
			return
		end

		@pilot.rating = certificate
		@pilot.instrument = instrument_rated
                @pilot.base = base_icao

		@airplane = Airplane.create(tailnumber: airplane_tailnumber, vx: airplane_vx, vy: airplane_vy,
			vg: airplane_vg, make: airplane_make, max_ascent: "500", max_descent: "500", pilot_id: @pilot.id)

		@pilot.setup_complete = true

		# confirm to user that profile was saved
		if @pilot.save
			flash[:success] = "Your information was saved. You may now create new flights"
			redirect_to "/plans/new"
		else
			render "initial"
			return
		end
	end

	# Create accunt
	def create
		@pilot = Pilot.create(pilot_params)
		@pilot.name = params[:firstname] + " " + params[:lastname]
		@years = 1999.downto(1935)
    @months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    @days = 1..31
    if @pilot.save
    	flash[:success] = "Your account was created successfully"
    	session[:pilot_id] = @pilot.id
    	redirect_to "/pilots/#{@pilot.id}/initial"
    else
    	flash[:danger] = "Please fix the following errors"
    	@pilot.destroy
    	render "new"
    	return
    end
	end

	def edit
		@pilot = Pilot.find(params[:id])
	end


	# update user (used in settings page)
	def update
		@pilot = Pilot.find(params[:id])
		if params[:firstname] != nil && params[:lastname] != nil && params[:firstname] != "" && params[:lastname] != ""
			@pilot.name = params[:firstname] + " " + params[:lastname]
		end

		if params[:rating] && is_number(params[:rating])
			if params[:rating].to_i >= 0 && params[:rating].to_i <= 4
				@pilot.rating = params[:rating].to_i
			end
		end

		if params[:instrument]
			if params[:instrument] == "true"
				@pilot.instrument = true
			else
				@pilot.instrument = false
			end
		end

		if @pilot.update(pilot_params)
			flash[:success] = "Your information was updated successfully."
			redirect_to '/settings#personal'
			return
		else
			flash[:danger] = "An error occurred while updating your information."
			redirect_to '/settings#personal'
			return
		end
	end

	def destroy
	end

	private
		def pilot_params
			params.require(:pilot).permit(:firstname, :lastname, :email, :password, :dob, :base, :rating, :instrument)
	  end

	  def require_same_user
	  	require_user
	  	if session[:pilot_id] != current_user.id
	  		redirect_to root_path
	  	end
	  end

end
