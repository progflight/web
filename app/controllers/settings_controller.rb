class SettingsController < ApplicationController

	before_action :require_complete_user

	def index
		@pilot = Pilot.find(session[:pilot_id])
		@airplane = @pilot.airplanes.first
	end

end