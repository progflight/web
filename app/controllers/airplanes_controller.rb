# Stors the pilot's (user's) airplan in the SQL database.
# The backend stores airplanes in a seperat table from the user
# to allow a user to have several airplanes (although not yet implemented on front end)
 

class AirplanesController < ApplicationController
	before_action :require_owner_pilot

	def edit
		@airplane = Airplane.find(params[:id])
	end

	def update
		@airplane = Airplane.find(params[:id])
	end

	private

	def airplane_params
		params.require(:airplane).permit(:tailnumber, :vx, :vy, :vg, :make)
	end

	def require_owner_pilot
		if !logged_in?
			flash[:danger] = "You must be logged in to perform that action"
			redirect_to root_path
		end
	end

end