class WelcomeController < ApplicationController

	before_action :require_check_user, only: [:index]

  def index
  end

  def terms
  end

  private
  	def require_check_user
  		if logged_in?
  			redirect_to '/plans/new'
  		end
  	end

end
