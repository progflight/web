# root application controller. Provides globaly accessible helper methods

class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  #Globaly accessible helper methods
  helper_method :current_user, :logged_in?, :require_user, :require_complete_user, :require_logged_out, :is_number

  # Current user on the session
  def current_user
  	@current_user ||= Pilot.find(session[:pilot_id]) if session[:pilot_id]
  end

  # is the current user logged in?
  def logged_in?
    if current_user != nil
      return true
    else
      return false
    end
  end

  # verifies that the given input is a number
  def is_number(string)
    true if Integer(string) rescue false
  end

  # Notifies user of the application that the action is only available for logged-in users
  def require_user
    if !logged_in?
      flash[:danger] = "You must be logged in to perform that action."
      redirect_to root_path
      return
    end
  end

  # Notifies the user of the application that the action is only available for
  # loged-in users who has finished their profile setup
  def require_complete_user
    if !logged_in?
      flash[:danger] = "You must be logged in to perform that action."
      redirect_to root_path
      return
    end
    if !current_user.setup_complete
      flash[:warning] = "Please complete your profile"
      redirect_to '/pilots/' + current_user.id.to_s + '/initial'
    end
  end

  #Notifies user of application that the action is only available if you are logged in
  def require_logged_out
    if logged_in?
      redirect_to root_path
    end
  end


end
