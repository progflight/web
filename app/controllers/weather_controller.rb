class WeatherController < ApplicationController

  # API call to get weather at destination and departure airport
  def airport_identifier
    if params[:icao] != nil
      require 'open-uri'
      url = "https://aviationweather.gov/adds/dataserver_current/httpparam?dataSource=metars&requestType=retrieve&format=xml&hoursBeforeNow=3&mostRecent=true&stationString="
      url += params[:icao]
      xmlResponse = open(url).read
      jsonResponse = Crack::XML.parse(xmlResponse)

      begin
        if jsonResponse['response']['data']['num_results'] == '0'
          render :json => 'error'
          return
        else
          render :json => jsonResponse['response']['data']
          return
        end
      rescue => e
        render :json => 'error'
      end

    else
      render :json => "error"
      return
    end
  end

end
