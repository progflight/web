# Generat a plan for the user

class PlansController < ApplicationController

	before_action :require_complete_user

	def index
	end

	def new
		@plan = Plan.new
	end

	def create
	end

	def destroy
	end

	def planFlight
		# check that all filds has been filled out
		if params[:departure] != nil && params[:destination] != nil && params[:altitude] != nil && params[:speed] != nil
			departure = params[:departure]
			destination = params[:destination]
			altitude = params[:altitude].to_i
			speed = params[:speed].to_i

			if departure.length == 4 and destination.length == 4
				# API call to generate plan
				headers = {'Authorization': Rails.application.config.auth_API, 'Content-Type': 'application/json'}
				uri = URI.parse("https://api.flightplandatabase.com/auto/generate")
				http = Net::HTTP.new(uri.host, uri.port)
				http.use_ssl = true
				data = {
					"fromICAO": departure,
					"toICAO": destination,
					"useNAT":"false",
					"usePACOT":"false",
					"useAWYLO":"true",
					"useAWYHI":"false",
					"cruiseAlt": altitude,
					"cruiseSpeed": speed
				}
				request = Net::HTTP::Post.new(uri.path, headers)
				request.body = data.to_json
				resp = http.request(request)
				# If 200 Ok, look at response id field
				if resp.response.code == "201" || resp.response.code == "200"
					hash_as_string = resp.body
					tresp = JSON.parse hash_as_string.gsub('=>', ':')
					fid = tresp['id']

					uri = URI.parse('https://api.flightplandatabase.com/plan/' + fid.to_s)

					# Request the generated plan as a PDF
					request = Net::HTTP::Get.new(uri.path, {'Accept' => 'application/pdf'})
					r = http.request(request)
					if r.response.code == "200"
						# save PDF
						f = File.new('public/' + fid.to_s + '.pdf', 'w')
						f.write(r.body.force_encoding('UTF-8'))
						f.close
						# render link to PDF on front end
						render :json => {'link' => '/' + fid.to_s + '.pdf'}
						return
					else
						render :json => 'error'
						return
					end
					
				else
					render :json => 'error'
					return
				end
				render :json => {'plan': '1'} 
				return
			else
				render :json => 'error'
				return
			end
		else
			render :json => 'error'
			return
		end
		
	end

	
end