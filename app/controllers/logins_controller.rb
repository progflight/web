# Controller for logg-in page

class LoginsController < ApplicationController

	# Do not allow a user to log-in while currently logged in
	before_action :require_logged_out, except: [:destroy]

	def new
	end

	# Check that user exist in DB and that password matches, put pilot ID on session
	def create
		pilot = Pilot.find_by(email: params[:email])

		if pilot && pilot.authenticate(params[:password])
			session[:pilot_id] = pilot.id
			redirect_to '/plans/new'
		else
			flash[:danger] = "Invalid email/password combination"
			redirect_to login_path
		end
	end

	# log-in with twitter to user's profile or generate a new user in DB if one does not exist
	def twitter
		begin
			@pilot = Pilot.find_or_create_from_auth_hash(auth_hash)
			session[:pilot_id] = @pilot.id
			redirect_to '/plans/new'
			return
		rescue => e
			flash[:warning] = 'You already have an account'
			redirect_to '/login'
			return
		end
	end

	# Resets the session if the user log out
	def destroy
		reset_session
		redirect_to root_path
	end

	protected

	# twitter endpoint for authentication (uses twitter secrets in request.env)
	def auth_hash
		request.env['omniauth.auth']
	end

end