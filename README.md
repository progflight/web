# CS 411 Project

Authors: Dharmesh Tarapore <dharmesh@bu.edu>, Vincent Wahl <vinwah@bu.edu>, Biken Maharjan <bm181354@bu.edu>, and Oliver Thorn <thornow@bu.edu>

Project is currently hosted on an AWS instance, which is accessible at:
http://ec2-18-217-7-65.us-east-2.compute.amazonaws.com

We use <a href="https://gitlab.com/progflight/web/boards?=" target="_blank">Gitlab Issues</a> to track bugs and assign them to individual members instead of a spreadsheet.

Form validation test spreadsheets are available in the `Team Assignment 4/5` directory.

Our database uses the <a href="https://en.wikipedia.org/wiki/Active_record_pattern" target="_blank">ActiveRecord pattern</a> to store and manipulate information. An entity relationship diagram is available in `./Team Assignment 4:5/erd.pdf`.

Specifics:

* Ruby version: 2.4.2

* System dependencies: Make sure you have the latest stable version of Ruby and Rails installed.

* Configuration: Requires valid API key from flightplandatabase.com/settings 

* Database initialization: 'rake db:schema:load'

* How to run the test suite: `bundle exec rake test` 

* Deployment instructions: run `cap production deploy` from the `master` branch
