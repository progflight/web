Our project is available here: https://gitlab.com/progflight/web

For issues and bugs, we use the GitLab issues board here: https://gitlab.com/progflight/web/boards?=
instead of spreadsheets.

The unit tests of setting up profile can be found in ./../test/models/pilot_test.rb