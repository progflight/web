# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171203001229) do

  create_table "airplanes", force: :cascade do |t|
    t.string "tailnumber"
    t.string "vx"
    t.string "vy"
    t.string "vg"
    t.string "va"
    t.string "make"
    t.integer "pilot_id"
    t.string "max_ascent"
    t.string "max_descent"
  end

  create_table "pilots", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.boolean "instrument", default: false
    t.integer "rating"
    t.string "password"
    t.string "password_digest"
    t.boolean "setup_complete", default: false
    t.string "dob"
    t.boolean "from_twitter", default: false
    t.string "provider", default: "standalone"
    t.string "token"
    t.string "secret"
    t.string "uid"
    t.string "base"
    t.string "role", default: "user"
  end

  create_table "plans", force: :cascade do |t|
    t.string "origin_latitude"
    t.string "origin_longitude"
    t.string "destination_latitude"
    t.string "destination_longitude"
    t.string "origin"
    t.string "destination"
    t.datetime "departure_time"
  end

  create_table "waypoints", force: :cascade do |t|
    t.string "latitude"
    t.string "longitude"
  end

end
