class AddDobToPilots < ActiveRecord::Migration[5.1]
  def change
    add_column :pilots, :dob, :string
  end
end
