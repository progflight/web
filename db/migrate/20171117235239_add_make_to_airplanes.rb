class AddMakeToAirplanes < ActiveRecord::Migration[5.1]
  def change
    add_column :airplanes, :make, :string
  end
end
