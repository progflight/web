class AddFromTwitterToPilots < ActiveRecord::Migration[5.1]
  def change
    add_column :pilots, :from_twitter, :boolean, default: :false
  end
end
