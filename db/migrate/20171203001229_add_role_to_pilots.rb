class AddRoleToPilots < ActiveRecord::Migration[5.1]
  def change
	add_column :pilots, :role, :string, default: "user"
  end
end
