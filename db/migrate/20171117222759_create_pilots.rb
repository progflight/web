class CreatePilots < ActiveRecord::Migration[5.1]
  def change
    create_table :pilots do |t|
      t.string :name, :email
      t.boolean :instrument, default: :false
      t.integer :rating
    end
  end
end
