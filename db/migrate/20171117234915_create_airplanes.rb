class CreateAirplanes < ActiveRecord::Migration[5.1]
  def change
    create_table :airplanes do |t|
      t.string :tailnumber, :vx, :vy, :vg, :va
    end
  end
end
