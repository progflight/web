class AddPasswordDigestToPilots < ActiveRecord::Migration[5.1]
  def change
    add_column :pilots, :password_digest, :string
  end
end
