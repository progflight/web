class AddUidToPilots < ActiveRecord::Migration[5.1]
  def change
    add_column :pilots, :uid, :string
  end
end
