class AddPasswordAndConfirmationToPilots < ActiveRecord::Migration[5.1]
  def change
    add_column :pilots, :password, :string
    add_column :pilots, :password_confirmation, :string
  end
end
