class AddTokenAndSecretToPilots < ActiveRecord::Migration[5.1]
  def change
    add_column :pilots, :provider, :string, default: 'standalone'
    add_column :pilots, :token, :string
    add_column :pilots, :secret, :string
  end
end
