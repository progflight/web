class CreatePlan < ActiveRecord::Migration[5.1]
  def change
    create_table :plans do |t|
      t.string :origin_latitude, :origin_longitude, :destination_latitude, :destination_longitude, :origin, :destination
      t.timestamp :departure_time
    end
  end
end
