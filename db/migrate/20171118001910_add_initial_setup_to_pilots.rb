class AddInitialSetupToPilots < ActiveRecord::Migration[5.1]
  def change
    add_column :pilots, :setup_complete, :boolean, default: :false
  end
end
