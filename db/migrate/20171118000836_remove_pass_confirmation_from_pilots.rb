class RemovePassConfirmationFromPilots < ActiveRecord::Migration[5.1]
  def change
    remove_column :pilots, :password_confirmation
  end
end
