class AddMaxAscentAndDescentToAirplanes < ActiveRecord::Migration[5.1]
  def change
    add_column :airplanes, :max_ascent, :string
    add_column :airplanes, :max_descent, :string 
  end
end
