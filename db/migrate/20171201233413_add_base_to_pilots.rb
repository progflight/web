class AddBaseToPilots < ActiveRecord::Migration[5.1]
  def change
    add_column :pilots, :base, :string
  end
end
