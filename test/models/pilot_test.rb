require 'test_helper'

class PilotTest < ActiveSupport::TestCase

	def setup
		@pilot = Pilot.new(name: "John Appleseed", email: "john@apple.com", password: "testpassword", rating: 0, instrument: true)
	end

	test "Pilot should be valid" do 
		assert @pilot.valid?
	end

	test "name should be present" do
		@pilot.name = " "
		assert_not @pilot.valid?
	end

	test "email address should be unique" do
		dup_pilot = @pilot.dup 
		dup_pilot.email = @pilot.email.upcase
		@pilot.save
		assert_not dup_pilot.valid?
	end

	test "email address should accept valid email addresses" do
		valid_addresses = %w[user@eee.com R_TDD-DS@eee.hello.org user@example.com first.last@eem.au laura+joe@monk.com]
		valid_addresses.each do |va|
			@pilot.email = va
			assert @pilot.valid?, '#{va.inspect} should be valid'
		end
	end

	test "email validation should reject invalid email addresses" do
		invalid_addresses = %w[user@example,com user_at_eee.org user.name@example. eee@i_am_.com]
		invalid_addresses.each do |ia|
			@pilot.email = ia 
			assert_not @pilot.valid?, '#{ia.inspect} should be invalid'
		end
	end

	test "password should not be too short" do
		short_password = "aa"
		@pilot.password = short_password
		assert_not @pilot.valid?
	end

	test "password should be within bounds" do
		out_of_bounds_password = "a" * 91
		@pilot.password = out_of_bounds_password
		assert_not @pilot.valid?
	end

end