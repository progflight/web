Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'welcome#index'

  get '/terms', to: 'welcome#terms'

  get '/privacy', to: 'welcome#privacy'

  get '/settings', to: 'settings#index'

  get '/weather/info', to: 'weather#airport_identifier'

  get '/login', to: 'logins#new'
  post '/login', to: 'logins#create'
  get '/logout', to: 'logins#destroy'

  get '/signup', to: 'pilots#new'

  post '/pilots', to: 'pilots#create'
  get '/pilots/:id/initial', to: 'pilots#initial'
  get '/pilots/:id', to: 'pilots#show'
  post '/pilots/:id/initial', to: 'pilots#initial_update'
  post '/pilots/:id', to: 'pilots#update'

  get '/plans/new', to: 'plans#new'

  # 3rd Party Authentication
  get '/auth/:provider/callback', to: 'logins#twitter'

  # API routes go below
  post '/api/v1/plan', to: 'plans#planFlight'

end
